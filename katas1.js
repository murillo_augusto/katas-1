function oneThroughTwenty() {
  for (let i = 1; i <= 20; i++) {
    console.log(i);
  }
}

console.log(oneThroughTwenty());
// //call function oneThroughTwenty

function evensToTwenty() {
  for (let i = 1; i <= 20; i++) {
    if ((i % 2) === 0) {
      console.log(i);
    }
  }
}

console.log(evensToTwenty());
// //call function evensToTwenty

function oddsToTwenty() {
  for (let i = 0; i <= 20; i++) {

    if ((i % 2) === 1) {
      console.log(i);
    }
  }
}
console.log(oddsToTwenty());
// //call function oddsToTwenty

function multiplesOfFive() {
  let i = 1;
  while (i * 5 <= 100) {
    console.log(i * 5);
    i++;
  }
}

console.log(multiplesOfFive());
// //call function multiplesOfFive

function squareNumbers() {
  let i = 1;
  while (i * i <= 100) {

    console.log(i * i);
    i++;
  }
}
console.log(squareNumbers());
// //call function squareNumbers

function countingBackwards() {
  for (let i = 0; i <= 20; -i++) {
    console.log(20 - i);
  }
}
console.log(countingBackwards());
// // //call function countingBackwards

function evenNumbersBackwards() {
  for (let i = 0; i <= 20; i++) {
    if (i % 2 === 0) {
      console.log(20 - i);
    }
  }

}
console.log(evenNumbersBackwards());
//call function evenNumbersBackwards

function oddNumbersBackwards() {
  for (let i = 0; i <= 20; i++) {
    if (i % 2 === 1) {
      console.log(20 - i);
    }
  }
}
console.log(oddNumbersBackwards());
//call function oddNumbersBackwards

function multiplesOfFiveBackwards() {
  let i = 1;
  let multipleFive = [];

  while (i * 5 <= 100) {
    let mutiple = i * 5;
    multipleFive.push(mutiple);
    i++;
  }
  console.log(multipleFive.reverse());
}

console.log(multiplesOfFiveBackwards());
//call function multiplesOfFiveBackwards

function squareNumbersBackwards() {

  let i = 1;
  let numbers = [];
  while (i * i <= 100) {
    let square = i * i;
    numbers.push(square);
    i++;
  }
  console.log(numbers.reverse());
}
console.log(squareNumbersBackwards());
//call function squareNumbersBackwards
